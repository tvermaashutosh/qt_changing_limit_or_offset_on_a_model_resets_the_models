#include "firstclass.h"

FirstClass::FirstClass(QObject* parent)
    : QObject(parent)
{}

FirstClass::FirstClass(const QString& name1, QObject* parent)
    : QObject(parent)
    , m_name1(name1)
{}

QString FirstClass::getName1() const
{
    return m_name1;
}

void FirstClass::setName1(const QString& name1)
{
    if(m_name1 != name1)
    {
        m_name1 = name1;
        emit name1Changed(m_name1);
    }
}
