#ifndef SECONDCLASS_H
#define SECONDCLASS_H

#include <QObject>
#include "firstclass.h"

class SecondClass : public QObject
{
    Q_OBJECT

public:
    SecondClass(QObject* parent = nullptr);
    SecondClass(const QString& name2, QObject* parent = nullptr);

    QString getName2() const;
    void setName2(const QString& name2);

public slots:
    void onName1Changed(const QString& name1);

private:
    QString m_name2;
};

#endif // SECONDCLASS_H
