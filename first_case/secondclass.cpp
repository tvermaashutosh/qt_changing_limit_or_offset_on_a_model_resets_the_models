#include "secondclass.h"

SecondClass::SecondClass(QObject* parent)
    : QObject(parent)
{}

SecondClass::SecondClass(const QString& name2, QObject* parent)
    : QObject(parent)
    , m_name2(name2)
{}

QString SecondClass::getName2() const
{
    return m_name2;
}

void SecondClass::setName2(const QString& name2)
{
    if(m_name2 != name2)
    {
        m_name2 = name2;
    }
}

void SecondClass::onName1Changed(const QString& name1)
{
    if(m_name2 != name1)
    {
        m_name2 = name1;
    }
}
