#include <QCoreApplication>
#include <QDebug>

#include "firstclass.h"
#include "secondclass.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    QString temp("Winnie the Pooh");

    std::unique_ptr<FirstClass> f = std::make_unique<FirstClass>(temp);
    std::unique_ptr<SecondClass> s = std::make_unique<SecondClass>( std::move(temp) );

    qDebug() << (f->getName1()); // Winnie the Pooh
    qDebug() << (s->getName2()); // Winnie the Pooh

    QObject::connect(f.get(), &FirstClass::name1Changed, s.get(), &SecondClass::onName1Changed);

    // Second Case

    QObject::connect(s.get(), &SecondClass::name2Changed, f.get(), &FirstClass::onName2Changed);

    s->setName2("Mickey Mouse");

    qDebug() << (f->getName1()); // Mickey Mouse
    qDebug() << (s->getName2()); // Mickey Mouse

    return a.exec();
}
