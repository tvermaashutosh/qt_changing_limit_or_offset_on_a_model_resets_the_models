#ifndef FIRSTCLASS_H
#define FIRSTCLASS_H

#include <QObject>

class FirstClass : public QObject
{
    Q_OBJECT

public:
    FirstClass(QObject* parent = nullptr);
    FirstClass(const QString& name1, QObject* parent = nullptr);

    QString getName1() const;
    void setName1(const QString& name1);

signals:
    void name1Changed(const QString& name1);

public slots:
    void onName2Changed(const QString& name2);

private:
    QString m_name1;
};

#endif // FIRSTCLASS_H
